import 'package:cres/screens/students.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _isValidate = false;
  void validate() {
    if (formKey.currentState!.validate()) {
      setState(() {
        _isValidate = true;
      });

      print("validated");
    } else {
      print("not validated");

      setState(() {
        _isValidate = false;
      });
    }
  }

  String? validatePass(value) {
    if (value != "admin") {
      return "Incorrect password";
    } else {
      return null;
    }
  }

  String? validateUser(value) {
    if (value != "admin") {
      return "User not found";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CircleAvatar(
                backgroundColor: Colors.black,
                radius: 70,
                child: CircleAvatar(
                  radius: 60,
                  child: Text(
                    "CRES",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Form(
                      key: formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            autovalidate: true,
                            validator: validateUser,
                            decoration: InputDecoration(
                              label: Text("User name"),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 5.0),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05,
                          ),
                          TextFormField(
                            autovalidate: true,
                            obscureText: true,
                            validator: validatePass,
                            decoration: InputDecoration(
                              label: Text("Password"),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide:
                                    BorderSide(color: Colors.red, width: 5.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                child: FloatingActionButton.extended(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onPressed: () {
                    validate();
                    _isValidate == true
                        ? Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DatabaseScreen()))
                        : () {};
                  },
                  label: Text("Login"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
