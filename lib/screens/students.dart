import 'package:cres/model/studentsModel.dart';
import 'package:cres/utils/database_helper.dart';
import "package:flutter/material.dart";

class DatabaseScreen extends StatefulWidget {
  const DatabaseScreen({Key? key}) : super(key: key);

  @override
  _DatabaseScreenState createState() => _DatabaseScreenState();
}

class _DatabaseScreenState extends State<DatabaseScreen> {
  int? selectedId;
  late bool _isEdit = false;
  final studentName = TextEditingController();

  final studentDob = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            margin: EdgeInsets.all(10),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: Text(
                    "Please enter student details Below",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      TextField(
                        decoration: InputDecoration(
                            label: Text("Student name"),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30))),
                        controller: studentName,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      TextField(
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                            label: Text("Date of birth"),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30))),
                        controller: studentDob,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: FloatingActionButton.extended(
                            onPressed: () async {
                              selectedId != null
                                  ? await DatabaseHelper.instance.update(
                                      Students(
                                          id: selectedId,
                                          name: studentName.text,
                                          dob: studentDob.text),
                                    )
                                  : await DatabaseHelper.instance.add(
                                      Students(
                                          name: studentName.text,
                                          dob: studentDob.text),
                                    );
                              setState(() {
                                _isEdit = false;
                                selectedId = null;
                                studentName.text = '';
                                studentDob.text = "";
                                studentName.clear();
                                studentDob.clear();
                              });
                            },
                            label: _isEdit == true
                                ? Text("Edit")
                                : Text("Submit")),
                      )
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Center(
                    child: FutureBuilder<List<Students>>(
                        future: DatabaseHelper.instance.getStudents(),
                        builder: (BuildContext contex,
                            AsyncSnapshot<List<Students>> snapshot) {
                          if (!snapshot.hasData) {
                            return Center(
                                child: const CircularProgressIndicator());
                          }
                          return snapshot.data!.isEmpty
                              ? Center(
                                  child: Text('No Products in list'),
                                )
                              : ListView(
                                  children: snapshot.data!.map((students) {
                                    return Center(
                                      child: Card(
                                        elevation: 10,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        color: selectedId == students.id
                                            ? Colors.white70
                                            : Colors.white,
                                        child: Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.1,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Container(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceAround,
                                                  children: [
                                                    Text(
                                                        "Name : ${students.name}"),
                                                    Text(
                                                        "DOB : ${students.dob}"),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceAround,
                                                  children: [
                                                    GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            _isEdit = true;

                                                            if (selectedId ==
                                                                null) {
                                                              studentName.text =
                                                                  students.name;
                                                              studentDob.text =
                                                                  students.dob;
                                                              selectedId =
                                                                  students.id;
                                                            } else {
                                                              studentName.text =
                                                                  '';
                                                              studentDob.text =
                                                                  "";
                                                              selectedId = null;
                                                            }
                                                          });
                                                        },
                                                        child:
                                                            Icon(Icons.edit)),
                                                    GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            DatabaseHelper
                                                                .instance
                                                                .remove(students
                                                                    .id!);
                                                            selectedId = null;
                                                            studentName.text =
                                                                '';
                                                            studentDob.text =
                                                                '';
                                                          });
                                                        },
                                                        child:
                                                            Icon(Icons.delete)),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                );
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
