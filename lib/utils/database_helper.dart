import 'dart:io';
import 'package:cres/model/studentsModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

// class DatabaseHelper {
//   DatabaseHelper._privateConsturactor();
//   static final DatabaseHelper instance = DatabaseHelper._privateConsturactor();

//   static Database? _database;
//   Future<Database> get database async => _database ??= await _initDatabase();

//   Future<Database> _initDatabase() async {
//     Directory documentsDirectory = await getApplicationDocumentsDirectory();
//     String path = join(documentsDirectory.path, 'etrade.db');
//     return await openDatabase(path, version: 1, onCreate: _onCreate);
//   }

//   Future _onCreate(Database db, int version) async {
//     await db.execute('''
//   CREATE TABLE students(
//       id INTEGER PRIMARY KEY,
//       name TEXT,
//       dob TEXT)
//     ''');
//   }

//   Future<List<Students>> getStudents() async {
//     Database db = await instance.database;
//     var students = await db.query(
//       'students',
//       orderBy: 'name',
//     );
//     List<Students> productList = students.isNotEmpty
//         ? students.map((c) => Students.fromMap(c)).toList()
//         : [];
//     return productList;
//   }

//   Future<int> add(Students student) async {
//     Database db = await instance.database;
//     return await db.insert('students', student.toMap());
//   }

//   Future<int> remove(int id) async {
//     Database db = await instance.database;
//     return await db.delete('students', where: 'id = ?', whereArgs: [id]);
//   }

//   Future<int> update(Students student) async {
//     Database db = await instance.database;
//     return await db.update('students', student.toMap(),
//         where: 'id = ?', whereArgs: [student.id]);
//   }
// }

class DatabaseHelper {
  DatabaseHelper._privateConsturactor();
  static final DatabaseHelper instance = DatabaseHelper._privateConsturactor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'cres.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
    CREATE TABLE students(
      id INTEGER PRIMARY KEY,
      name TEXT,
      dob TEXT)
    ''');
  }

  Future<List<Students>> getStudents() async {
    Database db = await instance.database;
    var students = await db.query('students', orderBy: 'name');
    List<Students> studentsList = students.isNotEmpty
        ? students.map((c) => Students.fromMap(c)).toList()
        : [];
    return studentsList;
  }

  Future<int> add(Students students) async {
    Database db = await instance.database;
    return await db.insert('students', students.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('students', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(Students students) async {
    Database db = await instance.database;
    return await db.update('students', students.toMap(),
        where: 'id = ?', whereArgs: [students.id]);
  }
}
