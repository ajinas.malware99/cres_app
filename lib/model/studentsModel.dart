class Students {
  final int? id;
  final String name;
  final String dob;

  Students({this.id, required this.name, required this.dob});

  factory Students.fromMap(Map<String, dynamic> json) =>
      new Students(id: json['id'], name: json['name'], dob: json["dob"]);

  Map<String, dynamic> toMap() {
    return {'id': id, 'name': name , "dob" : dob};
  }
}
